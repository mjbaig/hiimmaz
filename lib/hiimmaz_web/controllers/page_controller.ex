defmodule HiimmazWeb.PageController do
  use HiimmazWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
