import React from "react";

import ActionOfBenevolanceComponent from './action-of-benevolance-component/action-of-benevolance-component';

import './volunteering-component.scss'

export default class VolunteeringComponent extends React.Component {
  constructor() {
    super();
    this.volunteeringDataList = volunteeringDataList;
  }

  getVolunteeringData() {
    let volunteeringDataListElements = [];
    this.volunteeringDataList.forEach((action, index) => {
      action.actionKey = `${action.actionTitle}-${index}`.replace(/\s/g, '');
      volunteeringDataListElements.push(
        <div key={action.actionKey}>
          <ActionOfBenevolanceComponent data={action}/>
        </div>
      );
    });
    return volunteeringDataListElements;
  }

  render() {
    return (
      <section className="resume-section">
        <h5>Volunteering</h5>
        <section className="title" />
        {this.getVolunteeringData()}
      </section>
    )
  }

}

let volunteeringDataList = [
  {
    actionTitle: "Capital One Coders",
    actionDate: "2018",
    actionDescription: "Went to a middle school to teach how to create an Adroid game using MIT App Inventor."
  }
];