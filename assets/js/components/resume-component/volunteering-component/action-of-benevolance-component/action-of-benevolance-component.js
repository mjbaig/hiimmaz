import React from "react";

import './action-of-benevolance-component.scss'

export default class ActionOfBenevolance extends React.Component {
  constructor(props) {
    super(props);
    this.actionTitle = props.data.actionTitle;
    this.actionDate = props.data.actionDate;
    this.actionDescription = props.data.actionDescription
  }

  render() {
    return (
      <section className="action-of-benevolance">
        <h6 className="action-title">{this.actionTitle}</h6>
        <h6 className="action-date">{this.actionDate}</h6>
        <p className="action-description">{this.actionDescription}</p>
      </section>

    )
  }

}
