import React from "react";

import './project-component.scss'

export default class ProjectComponent extends React.Component {
  constructor(props) {
    super(props);
    this.projectTitle = props.data.projectTitle;
    this.projectDate = props.data.projectDate;
    this.projectDescription = props.data.projectDescription
  }

  render() {
    return (
      <section className="personal-project-component">
        <h6 className="personal-project-title">{this.projectTitle}</h6>
        <h6 className="personal-project-date">{this.projectDate}</h6>
        <p className="personal-project-description">{this.projectDescription}</p>
      </section>

    )
  }

}
