import React from "react";

import ProjectComponent from './project-component/project-component';

import './personal-projects-component.scss'

export default class PersonalProjectsComponent extends React.Component {
  constructor() {
    super();
    this.personalProjectsDataList = personalProjectsDataList;
  }

  getPersonalProjectsData() {
    let personalProjectsDataListElements = [];
    this.personalProjectsDataList.forEach((project, index) => {
      project.projectKey = `${project.projectTitle}-${index}`.replace(/\s/g, '');
      personalProjectsDataListElements.push(
        <div key={project.projectKey}>
          <ProjectComponent data={project}/>
        </div>
      );
    });
    return personalProjectsDataListElements;
  }

  render() {
    return (
      <section className="resume-section">
        <h5>Other Projects</h5>
        <section className="title" />
        {this.getPersonalProjectsData()}
      </section>
    )
  }

}

let personalProjectsDataList = [
  {
    projectTitle: "Capital One Think Tank Competition",
    projectDate: "2017",
    projectDescription: "Finalist in company’s ThinkTank competition. Developed an Android game to teach high school to college age students about finances. Worked on UI, UI Design, and Client Side Development."
  }
];