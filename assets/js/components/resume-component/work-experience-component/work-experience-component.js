import React from "react";

import './work-experience-component.scss';

import WorkExperienceElementComponent from './work-experience-element/work-experience-element-component';

export default class WorkExperienceComponent extends React.Component {
  constructor(props) {
    super(props);

  }

  renderWorkExperience() {
    let workExperienceList = [];
    workExperienceData.forEach((workExperience, index) => {
      workExperience.key = 'work-experience:'+index;
      workExperienceList.push(
        <div key={workExperience.key}>
          <WorkExperienceElementComponent data={workExperience}/>
        </div>
      );
    });
    return workExperienceList;
  }

  render() {
    return (
      <section className="resume-section">
        <h5>Work Experience</h5>
        <section className="title" />
        {this.renderWorkExperience()}
      </section>
    )
  }

}

let workExperienceData = [
  {
    companyName: "Capital One",
    companyCity: "Plano, TX",
    myPosition: "Senior Software Engineer",
    whatIWasDoingList: [
      "Working on a multi team effort to revamp a monolithic legacy system by splitting it into micro services to improve scalability and maintainability. To speed up the process, the team created a generator, which used Oracle metadata to automate the creation of microservices. The generator was created using Python, and the Pandas library was used to traverse the Oracle metadata. A proof of concept for this generator was also created using Kotlin, where the Krangl framework was used to traverse the Oracle metadata."
    ]
  },
  {
    companyName: "Capital One",
    companyCity: "Plano, TX",
    myPosition: "Software Engineer",
    whatIWasDoingList: [
      "Part of a team that developed an agent facing application to manage mortgages and home equity loans. The application consolidated many of the agent facing applications into a single web application and aimed to improve productivity and reduce call volume. The stack included Spring, Spring Boot Microservices, NodeJS, AngularJS/2+ and Cassandra. Worked on frontend and backend development.",
      "Developed internal dashboard to monitor file batch file transfers, and ETL processes containing critical home mortgage data and to alert responsible parties of failures or possible failures before start of business. It allows the responsible parties to act proactively when there is a failure. The stack includes Spring, AngularJS, Cassandra, Splunk, Salesforce Rest APIs and Microsoft SQL Server. Worked on frontend and backend development.",
      "Troubleshooted enterprise wide login issues that impacted thousands of home loans customers using TeraData, Oracle DB and Splunk."
    ]
  },
  {
    companyName: "Capital One",
    companyCity: "Plano, TX",
    myPosition: "Data Engineer",
    whatIWasDoingList: [
      "Part of a team that automated daily file transfers to Hadoop for auto and home loans using shell scripting to kickstart the company’s new distributed platform.",
      "Wrote a Python script that transferred  200 50+ gb Teradata tables to Hadoop and validated file integrity after I noticed that about 10% table transfers were failing in the old script due to unrecognizable characters.\n" +
      "Created a multithreaded bash script that executed daily file validations, which decreased validation times significantly. It also abstracted more than 5 others tasks that had to be done manually before.",
      "Part of a team that rebuilt one of Capital One’s most important data models on the Hadoop platform, using Hive, Java and Cascading."
    ]
  }
];