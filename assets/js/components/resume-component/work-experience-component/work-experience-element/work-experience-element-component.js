import React from "react";

import './work-experience-element-component.scss';

export default class WorkExperienceElementComponent extends React.Component {

  constructor(props) {
    super(props);
    this.companyName = this.props.data.companyName;
    this.companyCity = this.props.data.companyCity;
    this.myPosition = this.props.data.myPosition;
    this.whatIWasDoingList = this.props.data.whatIWasDoingList;
    this.workExperienceKey = this.props.data.key;
  }

  generateWhatIWasDoing() {
    let whatIWasDoingULTags = [];
    this.whatIWasDoingList.forEach((whatIDid, index) => {
      whatIWasDoingULTags.push(<li key={this.workExperienceKey + 'what-i-did'+index}>{whatIDid}</li>)
    });
    return whatIWasDoingULTags;
  }

  render(){
    return (
      <section className="work_experience_element">
        <h6 className="company_name">{this.companyName}</h6>
        <h6 className="company_city">{this.companyCity}</h6>
        <h6 className="my_position">{this.myPosition}</h6>
        <ul className="what_i_was_doing">
          {this.generateWhatIWasDoing()}
        </ul>
      </section>
    );
  }

}