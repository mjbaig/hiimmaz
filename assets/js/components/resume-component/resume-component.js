import React from "react";

import './resume-component.scss';

import EducationComponent from './education-component/education-component';

import PersonalStatementComponent from './personal-statement-component/personal-statement-component';

import SkillsComponent from './skills-component/skills-component';

import WorkExperienceComponent from './work-experience-component/work-experience-component';

import VolunteeringComponent from './volunteering-component/volunteering-component';

import PersonalProjectsComponent from './personal-projects-component/personal-projects-component';
import InterestsComponent from "./interests-component/interests-component";

export default class ResumeComponent extends React.Component {
  constructor() {
    super();
  }

  render() {
    return (
      <section className="category-containers">
        <div className="personal-statement">
          <PersonalStatementComponent/>
        </div>
        <div className="education-component">
          <EducationComponent/>
        </div>
        <div className="work-experience-component">
          <WorkExperienceComponent/>
        </div>
        <div className="personal-projects-component">
          <PersonalProjectsComponent/>
        </div>
        <div className="volunteering-component">
          <VolunteeringComponent/>
        </div>
        <div className="skills-component">
          <SkillsComponent/>
        </div>
        {/*<div className="interests-component">*/}
          {/*<InterestsComponent/>*/}
        {/*</div>*/}
      </section>
    )
  }

}