import React from "react";

import './skills-component.scss'

import SkillComponent from './skill-component/skill-component';

let data = [30, 86, 168, 281, 303, 365];
let barHeight = 20;

let skillsList = [
  {
    skillName: 'Java',
    rating: '8'
  },
  {
    skillName: 'Kotlin',
    rating: '7'
  },
  {
    skillName: 'Javascript',
    rating:'8'
  },
  {
    skillName: 'Python',
    rating: '7'
  },
  {
    skillName: 'Elixir',
    rating: '5'
  },
  {
    skillName: 'Spring Framework',
    rating: '7'
  },
  {
    skillName: 'Nerves Framework',
    rating: '5'
  },
  {
    skillName: 'Pheonix Framework',
    rating: '5'
  },
  {
    skillName: 'SQL',
    rating: '7'
  },
  {
    skillName: 'Linux',
    rating: '9'
  },
  {
    skillName: 'Partially Filling Pie Charts',
    rating: '10'
  }
];

export default class SkillsComponent extends React.Component {
  constructor(props) {
    super(props);
    this.skillsList = skillsList;
  }

  getSkills() {
    let skillsListHtmlElements = [];
    this.skillsList.forEach((skill, index) => {
      skill.skillKey = `${skill.skillName}-${index}`;
      skillsListHtmlElements.push(<li key={skill.skillKey}><SkillComponent data={skill}/></li>)
    });
    return skillsListHtmlElements;
  }

  render() {
    return (
      <section className="resume-section">
        <h5>Skills</h5>
        <section className="title" />
        <ul className="skills-list">
          {this.getSkills()}
        </ul>
        <svg className="chart" width={200} height={500}/>
      </section>
    )
  }

}