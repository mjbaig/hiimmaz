import React from "react";

import './skill-rating-component.scss'

import * as d3 from 'd3';

let barHeight = 20;
let svgHeight = 30;
let svgWidth = 30;
let radius = Math.min(svgWidth, svgHeight) / 2;

export default class SkillRatingComponent extends React.Component {
  constructor(props) {
    super(props);
    this.rating = props.data.rating;
    this.ratingKey = `rating-${props.data.skillKey}`.replace(/\s/g, '');
  }

  componentDidMount() {

    let data = [this.rating, 10 - this.rating];

    let svg = d3.select(`.${this.ratingKey}`);
    let g = svg.append("g").attr("transform", `translate(${svgWidth / 2}, ${svgHeight / 2})`)
    let color = ["#FF8D2F", "#C2D4E1"];

    let pie = d3.pie()
      .sort(null)
      .value((d) => { return d;});
    let path = d3.arc()
      .outerRadius(radius)
      .innerRadius(radius/2);

    g.selectAll(`.${this.ratingKey}`)
      .data(pie(data))
      .enter()
      .append("g")
      .attr("class", `.${this.ratingKey}`)
      .append("path")
      .attr("d", path)
      .attr("fill", (d, i) => { return color[i]; });

  }

  shouldComponentUpdate() {

  }

  render() {
    return (
      <div className="skill-rating">
        <svg className={this.ratingKey} height={svgHeight} width={svgWidth}/>
      </div>
    )
  }

}