import React from "react";

import './skill-component.scss'

import SkillRatingComponent from './skill-rating-component/skill-rating-component';

export default class Skill extends React.Component {
  constructor(props) {
    super(props);
    this.skillName = this.props.data.skillName;
    this.skillRating = this.props.data.rating;
  }

  render() {
    return (
      <section className="skill-component">
        <div className="skill-name">{this.skillName}</div>
        <div className="skill-rating"><SkillRatingComponent data={this.props.data}/></div>
      </section>
    )
  }

}