import React from "react";


export default class PersonalStatementComponent extends React.Component {
  constructor(props) {
    super(props);

  }

  render() {
    return (
      <section className="resume-section">
        <h5>Personal Statement</h5>
        <section className="title" />
        <p>I'm a software engineer with a bachelors in Aerospace from the University of Texas at Austin.
          I'm currently on a backend development team at Capital One, which is converting a legacy backend into more manageable microservices on AWS.</p>
      </section>
    )
  }

}