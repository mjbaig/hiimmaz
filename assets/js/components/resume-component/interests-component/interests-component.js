import React from "react";


export default class InterestsComponent extends React.Component {
  constructor(props) {
    super(props);

  }

  render() {
    return (
      <section className="interests-component">
        <div className="football-interest">potato</div>
        <div className="basketball-interest" />
        <div className="music-interest" />
        <div className="tech-interest" />
        <div className="drawing-interest" />
      </section>
    )
  }

}