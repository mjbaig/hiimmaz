import React from "react";

import './education-component.scss';

export default class EducationComponent extends React.Component {
  constructor(props) {
    super(props);

  }

  render() {
    return (
      <section className="resume-section">
        <h5>Education</h5>
        <section className="title" />
        <div className="education-component">
          <div className="university-name">
            <p>University Of Texas at Austin</p>
          </div>
          <div className="university-city">
            <p>Austin, TX</p>
          </div>
          <div className="degree-information">
            <p>Bachelor of Science In Aerospace Engineering</p>
          </div>
          <div className="graduation-date">
            <p>May 2015</p>
          </div>
        </div>
      </section>

    )
  }

}