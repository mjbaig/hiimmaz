// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".
//
// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.

// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".
import "phoenix_html"

// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

// import socket from "./socket"

import React from "react";
import ReactDOM from "react-dom";

import HeaderComponent from "./components/header-component/header-component";
import ResumeComponent from "./components/resume-component/resume-component";

import 'app.scss';

class HelloReact extends React.Component {

  constructor(){
    super();
  }

  render() {
    return (
      <div>
        <div>
          <div className="site-title">
            {/*<p>insert image here</p>*/}
            <h1>hi im maz</h1>
          </div>
          {/*<div className="header-component">*/}
            {/*<HeaderComponent/>*/}
          {/*</div>*/}
          <ResumeComponent/>
          {/*<div className="footer-component">*/}
            {/*<section>footer</section>*/}
          {/*</div>*/}
        </div>
      </div>
    )
  }
}

ReactDOM.render(
  <HelloReact/>,
  document.getElementById("app")
);