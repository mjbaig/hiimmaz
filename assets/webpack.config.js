const path = require('path');

module.exports = function(env) {
  const production = process.env.NODE_ENV === 'production';

  return {
    devtool: production ? 'source-maps' : 'eval',

    devServer: {
      headers: {
        'Access-Control-Allow-Origin': '*'
      },
    },

    entry: './js/app.js',

    output: production
      ? {
        path: path.resolve(__dirname, '../priv/static/js'),
        filename: 'app.js',
        publicPath: '/',
      }
      : {
        path: path.resolve(__dirname, 'public'),
        filename: 'app.js',
        publicPath: 'http://localhost:8080/',
      },

    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          use: ['babel-loader']
        },
        {
          test: /\.scss$/,
          use: [{
            loader: "style-loader"
          }, {
            loader: "css-loader"
          }, {
            loader: "sass-loader"
          }]
        },
        {
          test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[ext]',
                outputPath: 'fonts/',
              }
            }
          ]
        },
        {
          test: /\.(jpg|jpeg|gif|png|svg)$/,
          exclude: [/node_modules/, /fonts/],
          loader:'url-loader?limit=1024&name=images/[name].[ext]'
        },
        {
          test: /\.(woff|woff2|eot|ttf|svg)$/,
          exclude: [/node_modules/, /images/],
          loader: 'url-loader?limit=1024&name=fonts/[name].[ext]'
        }
      ],
    },

    resolve: {
      modules: ['node_modules', path.resolve(__dirname, 'js')],
      extensions: ['.js'],
    },
  };
};