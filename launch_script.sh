
#!/bin/bash

# install latest version of docker the lazy way
curl -sSL https://get.docker.com | sh

# make it so you don't need to sudo to run docker commands
usermod -aG docker ubuntu

# install docker-compose
curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

mkdir -p /prod/app
curl -o /prod/app/docker-compose.yml https://gitlab.com/mjbaig/hiimmaz/raw/master/docker-compose.yml

curl -o /etc/systemd/system/hiimmaz-app.service https://gitlab.com/mjbaig/hiimmaz/raw/master/hiimmaz-app.service

docker-compose -f /prod/app/docker-compose.yml up -d